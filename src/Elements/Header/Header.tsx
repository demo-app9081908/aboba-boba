import {Link, useLocation} from "react-router-dom";

export const Header = () => {
  let {pathname} = useLocation();

return (
    <header>
        <ul className='menu'>
            <li className='menu__item'><Link className={pathname === "/" ? "active" : ''} to="/">Меню</Link></li>
            <li className='menu__item'><Link className={pathname === "/selection" ? "active" : ''} to="/selection">Подбор</Link></li>
            <li className='menu__item'><Link className={pathname === "/about" ? "active" : ''} to="/about">О нас</Link></li>
            <li className='menu__item'><Link className={pathname === "/cart" ? "active" : ''} to="/cart">Корзина</Link></li>
        </ul>
    </header>
);
}