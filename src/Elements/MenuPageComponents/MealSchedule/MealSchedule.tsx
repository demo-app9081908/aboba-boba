export const MealSchedule: () => JSX.Element = () => {
  return (
    <div className="button__block">
        <button className='button button__block__date'>Пн 20.01</button>
        <button className='button button__block__date'>Вт 21.01</button>
        <button className='button button__block__date'>Ср 22.01</button>
        <button className='button button__block__date active_day active'>Чт 23.01</button>
        <button className='button button__block__date active_day'>Пт 24.01</button>
        <button className='button button__block__date active_day'>Сб 25.01</button>
        <button className='button button__block__date active_day'>Вс 26.01</button>
    </div>
  )
}