import { addToCart, removeFromCart } from "../../../store/main-data/main-data"
import { useAppDispatch } from "../../../store/hooks/hooks"

export const Item: (props: any) => JSX.Element = (props) => {
    const dispatch = useAppDispatch()

    return (
        <div className="order__body__cart__body__item__body">
            <div className="order__body__cart__body__item__body__item border">
                <div className="order__body__cart__body__item__body__item__img">
                    <img src={props.img} alt={props.name} />
                </div>
                <div className="order__body__cart__body__item__body__item__info">
                    <h4>{props.name}</h4>
                    <p>{props.dishes}</p>
                    <span>{`Общий вес: ${props.weight}`}</span>
                </div>
                <div className="order__body__cart__body__item__body__item__price" style={{display: props.completeList ? "none" : "flex"}}>
                    {
                        !!props.days ? (
                            <span className="item-days">{`${props.days} день`}</span>
                        )
                        :
                        null
                    }
                    <h4>{`${props.price} р.`}</h4>
                    <div className="order__body__cart__body__item__body__item__price__count">
                        <input type="number" value={props.quantity} readOnly />
                        <div>
                            <button onClick={() => dispatch(removeFromCart(props.name))}>-</button>
                            <span></span>
                            <button onClick={() => dispatch(addToCart(props.name))}>+</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}