import React from "react"
import { useAppSelector } from "../../../store/hooks/hooks"

export const ItemHeaderComplete: () => JSX.Element = () => {
    const data = useAppSelector(state => state.MAIN)
    return (
        <div className="order__body__cart__body__item__header">
            <h5>Заказ на 20 января</h5>
            <span className="paid-header">Оплачено <span>{`${data.paidOrders}`}</span></span>
        </div>
    )
}