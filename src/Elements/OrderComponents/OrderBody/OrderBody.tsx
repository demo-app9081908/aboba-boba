import React from 'react';
import { Item } from '../Item';
import { OrderHeader } from '../OrderHeader';
import { ItemHeader } from '../ItemHeader';
import { ItemHeaderComplete } from '../ItemHeaderComplete';
import { useAppSelector } from '../../../store/hooks/hooks';

export const OrderBody: () => JSX.Element = () => {
    const data = useAppSelector(state => state.MAIN);
    const key = (pre: number) => {
        return `${ pre }_${ new Date().getTime() }`;
    }
    const styles: any = {
        textAlign: 'left',
        margin: '0',
        padding: '20px'
    }
    
    return (
        <div className="order__body__cart">
            <OrderHeader/>
            <div className="order__body__cart__body">
                <div className="order__body__cart__body__item">
                    <ItemHeader />
                    <div style={{padding: '0 25px'}}>
                        {
                            data.cart.length === 0 ? 
                            <h1 style={styles}>Ваша корзина пуста</h1>
                            :
                            data.cart.map((e: any, i: number) => (
                                <Item key={key(i)} {...e} />
                            ))
                        }
                    </div>
                </div>
                <div className="order__body__cart__body__item">
                    <ItemHeaderComplete />
                    <div style={{padding: '0 25px'}}>
                        {
                            data.completedOrders.map((e: any, i: number) => (
                                <Item key={key(i)} {...e} />
                            ))
                        }
                    </div>
                </div>
            </div>
        </div>
    )
  }