import React from "react"
import { useAppSelector } from "../../../store/hooks/hooks";
import {Item} from '../../../types/state'
export const OrderInfo: () => JSX.Element = () => {

    const data = useAppSelector(state => state.MAIN)
    const numberOfProducts = data.cart.length;
    const weight = data.cart.reduce((a: number, b: Item) => a + (b.weight * b.quantity), 0)
    const totalSum = data.cart.reduce((a: number, b: Item) => a + (b.price * b.quantity), 0)
    const beautifySum = totalSum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")

    return (
        <div className="order__body__info">
            <div className="order__body__info__data-delivery">
                <div className="btn-section">
                    <button>Перейти к оформлению</button>
                </div>
                <div className="delivery-section">
                    <div>
                        Доставка: <span>25 янв, ср, 16:00 &#9662;</span>
                    </div>
                </div>
                <div className="cart-section">
                    <div className="cart-header"><span>Ваша корзина</span><span>{numberOfProducts} товара &#x2022; {weight.toFixed(2)} кг</span></div>
                    <div className="products"><span>Товары ({numberOfProducts})</span><span>{beautifySum} Р</span></div>
                    <div className="discount"><span>Скидка</span><span>- 0 P</span></div>
                </div>
                <div className="total">
                    <span>Общая стоимость</span><span>{beautifySum} Р</span>
                </div>
            </div>
            <div className="order__body__info__data-days">
                <h4>Оплата сразу</h4><span className="right-arrow">&#62;</span>
                <div>
                    <button><span>2 дня</span></button>
                    <button><span>6 дней</span><br />Выгода 7%</button>
                    <button><span>12 дней</span><br />Выгода 7%</button>
                </div>
            </div>
            <div className="order__body__info__data-user">
                <div className="number">
                    <input type="number" placeholder="Введите номер тиелефона"/>
                    <span>Для уточнения деталей заказа и доставки</span>
                </div>
                <div className="address">
                    <h4>Выберете адрес доставки</h4>
                    <input type="number" placeholder="Введите адрес..."/>
                </div>
            </div>
        </div>
    )
  }