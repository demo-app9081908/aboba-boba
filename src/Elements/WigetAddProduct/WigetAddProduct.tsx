import Carousel from "react-multi-carousel-18";
import img from "../../Assets/image/img.png";
import "react-multi-carousel-18/lib/styles.css";

export const WigetAddProduct: (props: {setShowWiget: any}) => JSX.Element = (props) => {
  const {setShowWiget} = props;
  const responsive = {
      desktop: {
          breakpoint: { max: 3000, min: 1024 },
          items: 3,
          slidesToSlide: 2 // optional, default to 1.
      },
      tablet: {
          breakpoint: { max: 1024, min: 464 },
          items: 2,
          slidesToSlide: 1 // optional, default to 1.
      },
      mobile: {
          breakpoint: { max: 464, min: 0 },
          items: 1,
          slidesToSlide: 1 // optional, default to 1.
      }
  };
  return (
      <div className="wiget__add__product">
          <div className="wiget__add__product__content">
              <div className="wiget__add__product__header">
                  <h4>Выбрать завтрак 1 на 20 января</h4>
                  <button className='button'  onClick={() => {
                      setShowWiget(false)
                  }}>
                      <svg width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <circle cx="24.7487" cy="24.7488" r="17.5" transform="rotate(135 24.7487 24.7488)" fill="#FD9058"/>
                          <path d="M17.5051 30.1814L22.9378 24.7488L17.5051 19.3161L19.316 17.5052L24.7487 22.9379L30.1813 17.5052L31.9922 19.3161L26.5596 24.7488L31.9922 30.1814L30.1813 31.9923L24.7487 26.5597L19.316 31.9923L17.5051 30.1814Z" fill="white"/>
                      </svg>
                  </button>
              </div>
              <div className="wiget__add__product__body">
                  <Carousel
                      swipeable={true}
                      draggable={true}
                      showDots={true}
                      responsive={responsive}
                      ssr={false} // means to render carousel on server-side.
                      infinite={false}
                      autoPlay={false}
                      autoPlaySpeed={1000}
                      keyBoardControl={true}
                      customTransition="all 1s ease-in-out"
                      transitionDuration={500}
                      containerClass="carousel-container"
                      removeArrowOnDeviceType={["tablet", "mobile"]}
                      deviceType={"desktop"}
                      dotListClass="custom-dot-list-style"
                      itemClass="carousel-item-padding-40-px"
                  >
                      <div className="wiget__add__product__body__item">
                          <div className="wiget__add__product__body__item__img">
                              <img src={img} alt=""/>
                          </div>
                          <div className="wiget__add__product__body__item__info">
                              <h4>Ntcn</h4>
                              <p>Блинчики с сыром, салатом, помидором, огурцом, курицей, соусом</p>
                          </div>
                          <div className="wiget__add__product__body__item__price">
                              <button className="button" style={{width: "35%"}}>+ 200 р.</button>
                              <button className="button" style={{width: "55%"}}>Выбрать</button>
                          </div>
                      </div>
                      <div className="wiget__add__product__body__item">
                          <div className="wiget__add__product__body__item__img">
                              <img src={img} alt=""/>
                          </div>
                          <div className="wiget__add__product__body__item__info">
                              <h4>Ntcn</h4>
                              <p>Блинчики с сыром, салатом, помидором, огурцом, курицей, соусом</p>
                          </div>
                          <div className="wiget__add__product__body__item__price">
                              <button className="button" style={{width: "35%"}}>+ 200 р.</button>
                              <button className="button" style={{width: "55%"}}>Выбрать</button>
                          </div>
                      </div>
                      <div className="wiget__add__product__body__item">
                          <div className="wiget__add__product__body__item__img">
                              <img src={img} alt=""/>
                          </div>
                          <div className="wiget__add__product__body__item__info">
                              <h4>Ntcn</h4>
                              <p>Блинчики с сыром, салатом, помидором, огурцом, курицей, соусом</p>
                          </div>
                          <div className="wiget__add__product__body__item__price">
                              <button className="button" style={{width: "35%"}}>+ 200 р.</button>
                              <button className="button" style={{width: "55%"}}>Выбрать</button>
                          </div>
                      </div>
                      <div className="wiget__add__product__body__item">
                          <div className="wiget__add__product__body__item__img">
                              <img src={img} alt=""/>
                          </div>
                          <div className="wiget__add__product__body__item__info">
                              <h4>Ntcn</h4>
                              <p>Блинчики с сыром, салатом, помидором, огурцом, курицей, соусом</p>
                          </div>
                          <div className="wiget__add__product__body__item__price">
                              <button className="button" style={{width: "35%"}}>+ 200 р.</button>
                              <button className="button" style={{width: "55%"}}>Выбрать</button>
                          </div>
                      </div>
                      <div className="wiget__add__product__body__item">
                          <div className="wiget__add__product__body__item__img">
                              <img src={img} alt=""/>
                          </div>
                          <div className="wiget__add__product__body__item__info">
                              <h4>Ntcn</h4>
                              <p>Блинчики с сыром, салатом, помидором, огурцом, курицей, соусом</p>
                          </div>
                          <div className="wiget__add__product__body__item__price">
                              <button className="button" style={{width: "35%"}}>+ 200 р.</button>
                              <button className="button" style={{width: "55%"}}>Выбрать</button>
                          </div>
                      </div>
                      <div className="wiget__add__product__body__item">
                          <div className="wiget__add__product__body__item__img">
                              <img src={img} alt=""/>
                          </div>
                          <div className="wiget__add__product__body__item__info">
                              <h4>Ntcn</h4>
                              <p>Блинчики с сыром, салатом, помидором, огурцом, курицей, соусом</p>
                          </div>
                          <div className="wiget__add__product__body__item__price">
                              <button className="button" style={{width: "35%"}}>+ 200 р.</button>
                              <button className="button" style={{width: "55%"}}>Выбрать</button>
                          </div>
                      </div>
                      <div className="wiget__add__product__body__item">
                          <div className="wiget__add__product__body__item__img">
                              <img src={img} alt=""/>
                          </div>
                          <div className="wiget__add__product__body__item__info">
                              <h4>Ntcn</h4>
                              <p>Блинчики с сыром, салатом, помидором, огурцом, курицей, соусом</p>
                          </div>
                          <div className="wiget__add__product__body__item__price">
                              <button className="button" style={{width: "35%"}}>+ 200 р.</button>
                              <button className="button" style={{width: "55%"}}>Выбрать</button>
                          </div>
                      </div>
                      <div className="wiget__add__product__body__item">
                          <div className="wiget__add__product__body__item__img">
                              <img src={img} alt=""/>
                          </div>
                          <div className="wiget__add__product__body__item__info">
                              <h4>Ntcn</h4>
                              <p>Блинчики с сыром, салатом, помидором, огурцом, курицей, соусом</p>
                          </div>
                          <div className="wiget__add__product__body__item__price">
                              <button className="button" style={{width: "35%"}}>+ 200 р.</button>
                              <button className="button" style={{width: "55%"}}>Выбрать</button>
                          </div>
                      </div>
                      <div className="wiget__add__product__body__item">
                          <div className="wiget__add__product__body__item__img">
                              <img src={img} alt=""/>
                          </div>
                          <div className="wiget__add__product__body__item__info">
                              <h4>Ntcn</h4>
                              <p>Блинчики с сыром, салатом, помидором, огурцом, курицей, соусом</p>
                          </div>
                          <div className="wiget__add__product__body__item__price">
                              <button className="button" style={{width: "35%"}}>+ 200 р.</button>
                              <button className="button" style={{width: "55%"}}>Выбрать</button>
                          </div>
                      </div>
                      <div className="wiget__add__product__body__item">
                          <div className="wiget__add__product__body__item__img">
                              <img src={img} alt=""/>
                          </div>
                          <div className="wiget__add__product__body__item__info">
                              <h4>Ntcn</h4>
                              <p>Блинчики с сыром, салатом, помидором, огурцом, курицей, соусом</p>
                          </div>
                          <div className="wiget__add__product__body__item__price">
                              <button className="button" style={{width: "35%"}}>+ 200 р.</button>
                              <button className="button" style={{width: "55%"}}>Выбрать</button>
                          </div>
                      </div>
                      <div className="wiget__add__product__body__item">
                          <div className="wiget__add__product__body__item__img">
                              <img src={img} alt=""/>
                          </div>
                          <div className="wiget__add__product__body__item__info">
                              <h4>Ntcn</h4>
                              <p>Блинчики с сыром, салатом, помидором, огурцом, курицей, соусом</p>
                          </div>
                          <div className="wiget__add__product__body__item__price">
                              <button className="button" style={{width: "35%"}}>+ 200 р.</button>
                              <button className="button" style={{width: "55%"}}>Выбрать</button>
                          </div>
                      </div>
                      <div className="wiget__add__product__body__item">
                          <div className="wiget__add__product__body__item__img">
                              <img src={img} alt=""/>
                          </div>
                          <div className="wiget__add__product__body__item__info">
                              <h4>Ntcn</h4>
                              <p>Блинчики с сыром, салатом, помидором, огурцом, курицей, соусом</p>
                          </div>
                          <div className="wiget__add__product__body__item__price">
                              <button className="button" style={{width: "35%"}}>+ 200 р.</button>
                              <button className="button" style={{width: "55%"}}>Выбрать</button>
                          </div>
                      </div>
                      <div className="wiget__add__product__body__item">
                          <div className="wiget__add__product__body__item__img">
                              <img src={img} alt=""/>
                          </div>
                          <div className="wiget__add__product__body__item__info">
                              <h4>Ntcn</h4>
                              <p>Блинчики с сыром, салатом, помидором, огурцом, курицей, соусом</p>
                          </div>
                          <div className="wiget__add__product__body__item__price">
                              <button className="button" style={{width: "35%"}}>+ 200 р.</button>
                              <button className="button" style={{width: "55%"}}>Выбрать</button>
                          </div>
                      </div>
                      <div className="wiget__add__product__body__item">
                          <div className="wiget__add__product__body__item__img">
                              <img src={img} alt=""/>
                          </div>
                          <div className="wiget__add__product__body__item__info">
                              <h4>Ntcn</h4>
                              <p>Блинчики с сыром, салатом, помидором, огурцом, курицей, соусом</p>
                          </div>
                          <div className="wiget__add__product__body__item__price">
                              <button className="button" style={{width: "35%"}}>+ 200 р.</button>
                              <button className="button" style={{width: "55%"}}>Выбрать</button>
                          </div>
                      </div>
                      <div className="wiget__add__product__body__item">
                          <div className="wiget__add__product__body__item__img">
                              <img src={img} alt=""/>
                          </div>
                          <div className="wiget__add__product__body__item__info">
                              <h4>Ntcn</h4>
                              <p>Блинчики с сыром, салатом, помидором, огурцом, курицей, соусом</p>
                          </div>
                          <div className="wiget__add__product__body__item__price">
                              <button className="button" style={{width: "35%"}}>+ 200 р.</button>
                              <button className="button" style={{width: "55%"}}>Выбрать</button>
                          </div>
                      </div>
                      <div className="wiget__add__product__body__item">
                          <div className="wiget__add__product__body__item__img">
                              <img src={img} alt=""/>
                          </div>
                          <div className="wiget__add__product__body__item__info">
                              <h4>Ntcn</h4>
                              <p>Блинчики с сыром, салатом, помидором, огурцом, курицей, соусом</p>
                          </div>
                          <div className="wiget__add__product__body__item__price">
                              <button className="button" style={{width: "35%"}}>+ 200 р.</button>
                              <button className="button" style={{width: "55%"}}>Выбрать</button>
                          </div>
                      </div>
                      <div className="wiget__add__product__body__item">
                          <div className="wiget__add__product__body__item__img">
                              <img src={img} alt=""/>
                          </div>
                          <div className="wiget__add__product__body__item__info">
                              <h4>Ntcn</h4>
                              <p>Блинчики с сыром, салатом, помидором, огурцом, курицей, соусом</p>
                          </div>
                          <div className="wiget__add__product__body__item__price">
                              <button className="button" style={{width: "35%"}}>+ 200 р.</button>
                              <button className="button" style={{width: "55%"}}>Выбрать</button>
                          </div>
                      </div>
                      <div className="wiget__add__product__body__item">
                          <div className="wiget__add__product__body__item__img">
                              <img src={img} alt=""/>
                          </div>
                          <div className="wiget__add__product__body__item__info">
                              <h4>Ntcn</h4>
                              <p>Блинчики с сыром, салатом, помидором, огурцом, курицей, соусом</p>
                          </div>
                          <div className="wiget__add__product__body__item__price">
                              <button className="button" style={{width: "35%"}}>+ 200 р.</button>
                              <button className="button" style={{width: "55%"}}>Выбрать</button>
                          </div>
                      </div>
                      <div className="wiget__add__product__body__item">
                          <div className="wiget__add__product__body__item__img">
                              <img src={img} alt=""/>
                          </div>
                          <div className="wiget__add__product__body__item__info">
                              <h4>Ntcn</h4>
                              <p>Блинчики с сыром, салатом, помидором, огурцом, курицей, соусом</p>
                          </div>
                          <div className="wiget__add__product__body__item__price">
                              <button className="button" style={{width: "35%"}}>+ 200 р.</button>
                              <button className="button" style={{width: "55%"}}>Выбрать</button>
                          </div>
                      </div>
                      <div className="wiget__add__product__body__item">
                          <div className="wiget__add__product__body__item__img">
                              <img src={img} alt=""/>
                          </div>
                          <div className="wiget__add__product__body__item__info">
                              <h4>Ntcn</h4>
                              <p>Блинчики с сыром, салатом, помидором, огурцом, курицей, соусом</p>
                          </div>
                          <div className="wiget__add__product__body__item__price">
                              <button className="button" style={{width: "35%"}}>+ 200 р.</button>
                              <button className="button" style={{width: "55%"}}>Выбрать</button>
                          </div>
                      </div>
                  </Carousel>
              </div>
          </div>
      </div>
  )}