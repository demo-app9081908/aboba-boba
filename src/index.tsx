import React from 'react';
import ReactDOM from 'react-dom/client';
import { Header } from './Elements/Header';
import { store } from './store/store';
import { Provider } from 'react-redux';
import {
    MenuPage,
    SelectionPage,
    AboutPage,
    Order
} from './pages';
import {
    createBrowserRouter,
    RouterProvider
} from "react-router-dom";

import "./Assets/sass/default.sass";

const Page: (props:{component: JSX.Element}) => JSX.Element = (props) => {
    const {component} = props;
    return (
        <>
            <Header/>
            {component}
        </>
    )
}


const router = createBrowserRouter([
    {
        path: "/",
        element: <Page component={<MenuPage />} />,
    },
    {
        path: "/selection",
        element: (<Page component={<SelectionPage />} />),
    },
    {
        path: "/about",
        element: (<Page component={<AboutPage />} />),
    },
    {
        path: "/cart",
        element: (<Page component={<Order />} />),
    },
]);

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
    <Provider store={store}>
        <div className="App">
            <RouterProvider router={router} />
        </div>
    </Provider>
);
