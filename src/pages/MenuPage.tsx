import React, { useState } from 'react';
import { Order } from './Order';
import { MealCategory } from '../Elements/MenuPageComponents/MealCategory';
import { SubHeader } from '../Elements/MenuPageComponents/SubHeader';
import { MealSchedule } from '../Elements/MenuPageComponents/MealSchedule';
import { AddDrink } from '../Elements/MenuPageComponents/AddDrink';
import { Breakfast } from '../Elements/MenuPageComponents/Breakfast';
import { LowerButtons } from '../Elements/MenuPageComponents/LowerButtons'
import { WigetAddProduct } from '../Elements/WigetAddProduct';

export const MenuPage: () => JSX.Element = () => {
    const [showWiget, setShowWiget] = useState(false);
    const [showOrder, setShowOrder] = useState(false);
    return (
        <>
            <div className='container'>
                <MealCategory />
                <SubHeader />
                <MealSchedule />
                <div className="card__block">
                    <AddDrink setShowWiget={setShowWiget} />
                    <Breakfast setShowWiget={setShowWiget} />
                    <AddDrink setShowWiget={setShowWiget} />
                    <Breakfast setShowWiget={setShowWiget} />
                    <Breakfast setShowWiget={setShowWiget} />
                    <Breakfast setShowWiget={setShowWiget} />
                </div>
                <LowerButtons setShowOrder={setShowOrder} />
            </div>
        {showWiget && <WigetAddProduct setShowWiget={setShowWiget}/>}
        {showOrder && <Order />}
      </>
  )
}