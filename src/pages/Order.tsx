import React from 'react';
import { OrderBody } from '../Elements/OrderComponents/OrderBody';
import { OrderInfo } from '../Elements/OrderComponents/OrderInfo';

export const Order: () => JSX.Element = () => {
    return (
        <div className="order">
            <div className="order__body">
                <OrderBody />
                <OrderInfo />
            </div>
        </div>
    )
}