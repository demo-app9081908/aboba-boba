import img from '../../Assets/image/img.png';
  
export const cart = [
    {
      "img": img,
      "name": "План питания: Поддержание",
      "dishes": "1 день: завтра, полдник, обед, ужин",
      "weight": 2.3,
      "price": 1250,
      "quantity": 12,
      "calories": 0,
      "days": 21,
      "completeList": false
    },
    {
      "img": img,
      "name": "Завтрак: Солнечная глазунья",
      "dishes": "Блинчики с сыром, салатом, помидором, огурцом, курицей, соусом",
      "weight": 0.3,
      "price": 150,
      "quantity": 1,
      "calories": 255,
      "days": 0,
      "completeList": false
    }
];
  
export const completedOrders = [
    {
      "img": img,
      "name": "План питания: Поддержание",
      "dishes": "2 день: завтра, полдник, обед, ужин",
      "weight": 2.1,
      "price": 200,
      "quantity": 0,
      "calories": 0,
      "days": 0,
      "completeList": true
    }
]