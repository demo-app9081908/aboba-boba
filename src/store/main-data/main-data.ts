import { createSlice } from '@reduxjs/toolkit';
import { NameSpace, DEFAULT_CITY } from '../../const';
import { MainState } from '../../types/state';
import { cart, completedOrders } from '../data/data';

const initialState: MainState = {
  cart,
  completedOrders,
  paidOrders: 15000,
};

export const mainData = createSlice({
  name: NameSpace.user,
  initialState,
  reducers: {
    addToCart: (state, action) => {
      const item: any = state.cart.find(e => e.name === action.payload)
      item.quantity += 1
    },
    removeFromCart: (state, action) => {
      const item: any = state.cart.find(e => e.name === action.payload)
      item.quantity -= 1
      if (item.quantity === 0) {
        console.log('first')
        const newCart: any = state.cart.filter(e => e.name !== action.payload)
        state.cart = newCart
      }
    },
    getHotels: (state, action) => {
      // state.hotels = action.payload;
      // state.isLodaing = false;
    },
    setCity: (state, action) => {
      // state.city = action.payload;
    },
    setEmptryHotel: (state, action) => {
      // state.emptryHotel = action.payload;
      // state.isLodaing = false;
    },
  },
});

export const { getHotels, setCity, setEmptryHotel, addToCart, removeFromCart } = mainData.actions;
