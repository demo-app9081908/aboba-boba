import { combineReducers } from '@reduxjs/toolkit';
import { NameSpace } from '../const';
import { mainData } from './main-data/main-data';

export const rootReducer = combineReducers({
  [NameSpace.main]: mainData.reducer
});
